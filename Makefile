.PHONY: build deploy run

IMG_NAME = ashneo76/gpg
IMG_VERSION = v4
IMG_REGISTRY = registry.gitlab.com

build:
	docker build -t $(IMG_REGISTRY)/$(IMG_NAME):$(IMG_VERSION) .

deploy:
	docker push $(IMG_REGISTRY)/$(IMG_NAME):$(IMG_VERSION)

run:
	docker run $(IMG_NAME):$(IMG_VERSION)
