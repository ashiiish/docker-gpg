FROM debian:latest

RUN apt-get -qq update; apt-get -y install gnupg2 bash pass vim supervisor
RUN yes | adduser --disabled-password --home /home/gpg --shell /bin/bash gpg

ADD ./supervisord.conf /etc/supervisor/supervisord.conf
ADD ./supervisor /etc/supervisor/conf.d

RUN mkdir -p /etc/supervisor/conf.d \
            /var/log/supervisor \
            /var/run/supervisor
RUN chown -R gpg /var/run/supervisor; chown -R gpg /var/log/supervisor; chown -R gpg /etc/supervisor/conf.d

USER gpg
WORKDIR /home/gpg

CMD ["supervisord", "-c", "/etc/supervisor/supervisord.conf"]
